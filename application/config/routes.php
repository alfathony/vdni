<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'dashboard';

$route['module/purchase-order/(:any)/(:any)/(:any)'] = 'purchase_order/$1/$2/$3';
$route['module/purchase-order/(:any)/(:any)'] = 'purchase_order/$1/$2';
$route['module/purchase-order/(:any)'] = 'purchase_order/$1';
$route['module/purchase-order'] = 'purchase_order';

$route['module/bill-of-lading/(:any)/(:any)/(:any)/(:any)'] = 'bill_of_lading/$1/$2/$3/$4';
$route['module/bill-of-lading/(:any)/(:any)/(:any)'] = 'bill_of_lading/$1/$2/$3';
$route['module/bill-of-lading/(:any)/(:any)'] = 'bill_of_lading/$1/$2';
$route['module/bill-of-lading/(:any)'] = 'bill_of_lading/$1';
$route['module/bill-of-lading'] = 'bill_of_lading';

$route['module/vendor-master-data/(:any)/(:any)/(:any)'] = 'vendor_master_data/$1/$2/$3';
$route['module/vendor-master-data/(:any)/(:any)'] = 'vendor_master_data/$1/$2';
$route['module/vendor-master-data/(:any)'] = 'vendor_master_data/$1';
$route['module/vendor-master-data'] = 'vendor_master_data';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
