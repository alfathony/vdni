<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * 			
 */
class Bill_of_lading extends CI_Controller
{
	/**
	 * @author Alfathony
	 *
	 * 29 Okt 2018
	 */

	public $module = "bill_of_lading";

	function __construct(){

		parent::__construct();

		$this->load->model('bill_of_lading_model');
		$this->load->model('purchase_order_model');
	}

	public function index()
	{
		// declare meta

		$data['title'] = "Admin - " . $this->module;
		$data['module_name'] = $this->module;

		// load data

		$data['rows_data'] = $this->bill_of_lading_model->get();

		// load layout

		$this->load->view('common/meta', $data);
		$this->load->view('common/header', $data);
		$this->load->view('common/sidebar');
		$this->load->view('modules/' . $this->module . '/index', $data);
		$this->load->view('common/footer', $data);
	}

	public function detail($id)
	{
		// declare meta

		$data['title'] = "Admin - " . $this->module;
		$data['module_name'] = $this->module;

		// load data

		$data['data'] = $this->bill_of_lading_model->get_row($id); 
		$data['rows_data'] = [];
		foreach ($this->purchase_order_model->get_po_items('id_po', $data['data']['id_po']) as $key => $value) { 
			foreach ($this->bill_of_lading_model->get_bl_items('bill_of_lading_container.id_po_item', $value['id_po_items']) as $key => $bl) {
				$data['rows_data'][] = $bl;
			}
		}  

		// load layout

		$this->load->view('common/meta', $data);
		$this->load->view('common/header', $data);
		$this->load->view('common/sidebar');
		$this->load->view('modules/' . $this->module . '/detail', $data);
		$this->load->view('common/footer', $data);
	}

	public function add()
	{
		// declare meta

		$data['title'] = "Admin - " . $this->module;
		$data['module_name'] = $this->module;

		// load data

		$data['purchase_orders'] = $this->purchase_order_model->get();

		// load layout

		$this->load->view('common/meta', $data);
		$this->load->view('common/header', $data);
		$this->load->view('common/sidebar');
		$this->load->view('modules/' . $this->module . '/form', $data);
		$this->load->view('common/footer', $data);
	}

	public function store()
	{
		$no_bill = $this->input->post('no_bill_of_lading');

		// Check PO Number Available
		$checking = $this->bill_of_lading_model->get_count(array('no_bill' => $no_bill));

		if ($checking == 0) {
			// receive post data

			$data = array(
				'no_bl' => $no_bill, 
				'id_po' => $this->input->post('id_po'), 
				'shipper_name' => $this->input->post('shipper_name'),
				'consignee' => $this->input->post('consignee'),
				'date' => $this->input->post('date'),
				'vessel' => $this->input->post('vessel'),
				'voyage_no' => $this->input->post('no_voyage'),
				'port_of_loading' => $this->input->post('port_of_loading'),
				'port_of_discharge' => $this->input->post('port_of_discharge'),
				'notify_party' => $this->input->post('notify_party'),
				'no_original_bl' => $this->input->post('no_original_bill_of_lading'),
				'master_name' => $this->input->post('master_name')
			);


			// store to model
			$this->bill_of_lading_model->store($data);
			redirect('module/' . $this->uri->segment('2'));
		}else{
			$this->session->set_flashdata('error', "<div class='alert alert-danger'>
                                <strong>Ops!</strong> Number bill of lading exist. Please try again
                            </div>");
			redirect('module/' . $this->uri->segment('2') . '/add');
		} 

	}

	public function edit($id)
	{

		$checking = $this->bill_of_lading_model->get_count('id_bl',$id);

		if ($checking > 0) {
			$data['id'] = $id;

			// load data

			$data['data'] = $this->bill_of_lading_model->get_row($id);

			$data['purchase_orders'] = $this->purchase_order_model->get();


			// declare meta

			$data['title'] = "Admin Edit PO - " . $this->module;
			$data['module_name'] = $this->module;
			

			// load layout

			$this->load->view('common/meta', $data);
			$this->load->view('common/header', $data);
			$this->load->view('common/sidebar');
			$this->load->view('modules/' . $this->module . '/form', $data);
			$this->load->view('common/footer', $data);

		}else{
			redirect('module/' . $this->uri->segment('2'));
		}
	}

	public function update($id)
	{
		// receive post data

		$data = array(
			'no_bl' => $this->input->post('no_bill_of_lading'), 
			'id_po' => $this->input->post('id_po'), 
			'shipper_name' => $this->input->post('shipper_name'),
			'consignee' => $this->input->post('consignee'),
			'date' => $this->input->post('date'),
			'vessel' => $this->input->post('vessel'),
			'voyage_no' => $this->input->post('no_voyage'),
			'port_of_loading' => $this->input->post('port_of_loading'),
			'port_of_discharge' => $this->input->post('port_of_discharge'),
			'notify_party' => $this->input->post('notify_party'),
			'no_original_bl' => $this->input->post('no_original_bill_of_lading'),
			'master_name' => $this->input->post('master_name')
		);

		// return pre($data);

		// update to model
		$this->bill_of_lading_model->update($id,$data);
		redirect('module/' . $this->uri->segment('2') . '/detail/' . $id);

	}

	public function delete()
	{
		if (null !== $this->input->post('id')) {
			$this->bill_of_lading_model->delete($this->input->post('id'));
			echo "200";
		}else{
			echo "400";
		}
	}

	public function itemform($id_bl,$form,$id_bl_container=false)
	{
		// declare meta

		if ($form == "edit") {
			$data['title'] = "Edit Item container";
			$data['module_name'] = $this->module;
			$data['id_bl_container'] = $id_bl_container;
			$data['id_bl'] = $id_bl;
		    $data['subtitle'] = "Edit this bill of lading";
		    $data['action'] = "/update_item/" . $id_bl . "/" . $id_bl_container;
		    $data['data'] = $this->bill_of_lading_model->get_item_row($id_bl_container);

		    // load data
		    $data['dataBL'] = $this->bill_of_lading_model->get_row($id_bl);

		    // purchase items
		   	$data['purchase_items'] = $this->purchase_order_model->get_po_items('id_po', $data['dataBL']['id_po']); 

		}else{
			$data['title'] = "Add Item Container";
			$data['module_name'] = $this->module;
			$data['id'] = $id_bl;
		    // $data['data'] = $this->bill_of_lading_model->get_item_row($id);
		    $data['subtitle'] = "Add this bill of lading";
		    $data['action'] = "/store_item/" . $id_bl;

		    // load data
		    $data['dataBL'] = $this->bill_of_lading_model->get_row($id_bl);

		    // purchase items
		   	$data['purchase_items'] = $this->purchase_order_model->get_po_items('id_po', $data['dataBL']['id_po']);  
		}

		// load layout

		$this->load->view('common/meta', $data);
		$this->load->view('common/header', $data);
		$this->load->view('common/sidebar');
		$this->load->view('modules/' . $this->module . '/item_form', $data);
		$this->load->view('common/footer', $data);
	}

	public function store_item($id)
	{ 

		$data = array(
			'id_po_item' => $this->input->post('id_po_item'),
			'number_container' => $this->input->post('number_container'),
			'type_container' => $this->input->post('type_container'),
			'seal' => $this->input->post('seal'), 
			'ton' => $this->input->post('ton'),
			'ton_checked' => $this->input->post('ton_checked')
		); 

		// store to model
		$this->bill_of_lading_model->store_item($data);
		redirect('module/' . $this->uri->segment('2') . '/detail/' . $id);
	}

	public function update_item($id_bl, $id_bl_container)
	{
		// receive post data 
		$data = array(
			'id_po_item' => $this->input->post('id_po_item'),
			'number_container' => $this->input->post('number_container'),
			'type_container' => $this->input->post('type_container'),
			'seal' => $this->input->post('seal'), 
			'ton' => $this->input->post('ton'),
			'ton_checked' => $this->input->post('ton_checked')
		); 

		// update to model
		$this->bill_of_lading_model->update_item($id_bl_container,$data,$this->input->post('id_po'));
		redirect('module/' . $this->uri->segment('2') . '/detail/' . $id_bl);

	}

	public function delete_item($id)
	{
		$this->bill_of_lading_model->delete_item($id);
		redirect($_SERVER['HTTP_REFERER']);
	}

}