<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	/**
	 * @author Alfathony
	 *
	 * 29 Okt 2018
	 */

	public $module = "dashboard";

	function __construct(){

		parent::__construct();

		$this->load->model('purchase_order_model');
		$this->load->model('vendor_model');
	}

	public function index()
	{
		// declare meta

		$data['title'] = "Admin - " . $this->module;
		$data['module_name'] = $this->module;

		// load data

		$data['count_all_po'] = $this->purchase_order_model->get_count();
		$data['count_draft'] = $this->purchase_order_model->get_count(array('status' => 1));
		$data['count_otw'] = $this->purchase_order_model->get_count(array('status' => 2));
		$data['count_done'] = $this->purchase_order_model->get_count(array('status' => 3));

		// load layout

		$this->load->view('common/meta', $data);
		$this->load->view('common/header', $data);
		$this->load->view('common/sidebar');
		$this->load->view('modules/' . $this->module . '/index', $data);
		$this->load->view('common/footer', $data);
	}
}
