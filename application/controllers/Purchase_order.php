<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Purchase_order extends CI_Controller {

	/**
	 * @author Alfathony
	 *
	 * 29 Okt 2018
	 */

	public $module = "purchase_order";

	function __construct(){

		parent::__construct();

		$this->load->model('purchase_order_model');
		$this->load->model('vendor_model');
	}

	public function index()
	{
		// declare meta

		$data['title'] = "Admin - " . $this->module;
		$data['module_name'] = $this->module;

		// load data

		$data['rows_data'] = $this->purchase_order_model->get();

		// load layout

		$this->load->view('common/meta', $data);
		$this->load->view('common/header', $data);
		$this->load->view('common/sidebar');
		$this->load->view('modules/' . $this->module . '/index', $data);
		$this->load->view('common/footer', $data);
	}

	public function detail($id)
	{
		// declare meta

		$data['title'] = "Admin - " . $this->module;
		$data['module_name'] = $this->module;

		// load data

		$data['data'] = $this->purchase_order_model->get_row($id);
		$data['rows_data'] = $this->purchase_order_model->get_po_items('id_po', $id);
		$data['total'] = $this->purchase_order_model->getTotalPO($id);

		// load layout

		$this->load->view('common/meta', $data);
		$this->load->view('common/header', $data);
		$this->load->view('common/sidebar');
		$this->load->view('modules/' . $this->module . '/detail', $data);
		$this->load->view('common/footer', $data);
	}

	public function add()
	{
		// declare meta

		$data['title'] = "Admin - " . $this->module;
		$data['module_name'] = $this->module;

		// load data

		$data['vendors'] = $this->vendor_model->get();

		// load layout

		$this->load->view('common/meta', $data);
		$this->load->view('common/header', $data);
		$this->load->view('common/sidebar');
		$this->load->view('modules/' . $this->module . '/form', $data);
		$this->load->view('common/footer', $data);
	}


	public function store()
	{
		$po_no = $this->input->post('po_no');

		// Check PO Number Available
		$checking = $this->purchase_order_model->get_count(array('po_no' => $po_no));

		if ($checking == 0) {
			// receive post data

			$data = array(
				'po_no' => $po_no,
				'status' => 1, //draft
				'po_date' => $this->input->post('po_date'),
				'id_vendor' => $this->input->post('id_vendor'),
				'delivery_time' => $this->input->post('delivery_time'),
				'payment_method' => $this->input->post('payment_method'),
				'payment_term' => $this->input->post('payment_term'),
				'note' => $this->input->post('note'),
				'downpayment' => $this->input->post('downpayment'),
				'discount' => $this->input->post('discount'),
				'vat' => $this->input->post('vat'),
				'receiver_name' => $this->input->post('receiver_name'),
				'receiver_date' => $this->input->post('receiver_date'),
				'prefered_by' => $this->input->post('prefered_by'),
				'approved_by' => $this->input->post('approved_by'),
				'created_at' => date('Y-m-d H:i:s')
			);


			// store to model
			$this->purchase_order_model->store($data);
			redirect('module/' . $this->uri->segment('2'));
		}else{
			$this->session->set_flashdata('error', "<div class='alert alert-danger'>
                                <strong>Ops!</strong> Number purchase order exist. Please try again
                            </div>");
			redirect('module/' . $this->uri->segment('2') . '/add');
		}

	}


	public function edit($id)
	{

		$checking = $this->purchase_order_model->get_count('id_po',$id);

		if ($checking > 0) {
			$data['id'] = $id;

			// load data

			$data['data'] = $this->purchase_order_model->get_row($id);

			$data['vendors'] = $this->vendor_model->get();


			// declare meta

			$data['title'] = "Admin Edit PO - " . $this->module;
			$data['module_name'] = $this->module;
			

			// load layout

			$this->load->view('common/meta', $data);
			$this->load->view('common/header', $data);
			$this->load->view('common/sidebar');
			$this->load->view('modules/' . $this->module . '/form', $data);
			$this->load->view('common/footer', $data);

		}else{
			redirect('module/' . $this->uri->segment('2'));
		}
	}

	public function update($id)
	{
		// receive post data

		$data = array(
			'po_no' => $this->input->post('po_no'),
			'po_date' => $this->input->post('po_date'),
			'id_vendor' => $this->input->post('id_vendor'),
			'delivery_time' => $this->input->post('delivery_time'),
			'payment_method' => $this->input->post('payment_method'),
			'payment_term' => $this->input->post('payment_term'),
			'note' => $this->input->post('note'),
			'downpayment' => $this->input->post('downpayment'),
			'discount' => $this->input->post('discount'),
			'vat' => $this->input->post('vat'),
			'receiver_name' => $this->input->post('receiver_name'),
			'receiver_date' => $this->input->post('receiver_date'),
			'prefered_by' => $this->input->post('prefered_by'),
			'approved_by' => $this->input->post('approved_by'),
			'updated_at' => date('Y-m-d H:i:s')
		);

		// return pre($data);

		// update to model
		$this->purchase_order_model->update($id,$data);
		redirect('module/' . $this->uri->segment('2') . '/detail/' . $id);

	}

	public function delete()
	{
		if (null !== $this->input->post('id')) {
			$this->purchase_order_model->delete($this->input->post('id'));
			echo "200";
		}else{
			echo "400";
		}
	}

	public function changestatus()
	{
		$id_po = $this->input->post('id');
		$status = $this->input->post('status');

		if (null !== $this->input->post('id')) {
			$this->purchase_order_model->changestatus($id_po, $status);
			echo "200";
		}else{
			echo "400";
		}
	}


	public function itemform($id,$form)
	{
		// declare meta

		if ($form == "edit") {
			$data['title'] = "Admin - Edit Item " . $this->module;
			$data['module_name'] = $this->module;
			$data['id'] = $id;
		    $data['subtitle'] = "Edit this purchase order";
		    $data['action'] = "/update_item/" . $id;

		    // load data
		    $data['data'] = $this->purchase_order_model->get_item_row($id);

		}else{
			$data['title'] = "Admin - Add Item " . $this->module;
			$data['module_name'] = $this->module;
			$data['id'] = $id;
		    $data['subtitle'] = "Add this purchase order";
		    $data['action'] = "/store_item/" . $id;
		}

		// load layout

		$this->load->view('common/meta', $data);
		$this->load->view('common/header', $data);
		$this->load->view('common/sidebar');
		$this->load->view('modules/' . $this->module . '/item_form', $data);
		$this->load->view('common/footer', $data);
	}

	public function store_item($id)
	{
		// receive post data
		$qty = $this->input->post('qty');
		$unit_price = $this->input->post('unit_price');
		$disc = $this->input->post('disc');

		$total = $qty * $unit_price;
		$totalWithDiscount = getAfterDiscount($total, $disc);

		$data = array(
			'id_po' => $id,
			'item' => $this->input->post('item'),
			'item_code' => $this->input->post('item_code'),
			'part_no' => $this->input->post('part_no'),
			'qty' => $qty,
			'unit' => $this->input->post('unit'),
			'unit_price' => $unit_price,
			'disc' => $disc,
			'total' =>  $totalWithDiscount,
			'created_at' => date('Y-m-d H:i:s')
		);

		// return pre($data);

		// store to model
		$this->purchase_order_model->store_item($data);
		redirect('module/' . $this->uri->segment('2') . '/detail/' . $id);
	}

	public function update_item($id)
	{
		// receive post data

		$qty = $this->input->post('qty');
		$unit_price = $this->input->post('unit_price');
		$disc = $this->input->post('disc');

		$total = $qty * $unit_price;
		$totalWithDiscount = getAfterDiscount($total, $disc);

		$data = array(
			'item' => $this->input->post('item'),
			'item_code' => $this->input->post('item_code'),
			'part_no' => $this->input->post('part_no'),
			'qty' => $qty,
			'unit' => $this->input->post('unit'),
			'unit_price' => $unit_price,
			'disc' => $disc,
			'total' =>  $totalWithDiscount,
			'updated_at' => date('Y-m-d H:i:s')
		);

		// return pre($data);

		// update to model
		$this->purchase_order_model->update_item($id,$data,$this->input->post('id_po'));
		redirect('module/' . $this->uri->segment('2') . '/detail/' . $this->input->post('id_po'));

	}

	public function delete_item($id)
	{
		$this->purchase_order_model->delete_item($id);
		redirect($_SERVER['HTTP_REFERER']);
	}


	public function export()
	{ 
	    include APPPATH.'third_party/PHPExcel.php';
	    
	    $excel = new PHPExcel(); 
	    $excel->getProperties()->setCreator('My Notes Code')
	                 ->setLastModifiedBy('My Notes Code')
	                 ->setTitle("Purchase Order")
	                 ->setSubject("Purchase Order")
	                 ->setDescription("Purchase Order Data")
	                 ->setKeywords("Purchase Order");
	    
	    $style_col = array(
	      'font' => array('bold' => true),
	      'alignment' => array(
	        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, 
	        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
	      ),
	      'borders' => array(
	        'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), 
	        'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  
	        'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), 
	        'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) 
	      )
	    );

	    $style_row = array(
	      'alignment' => array(
	        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER 
	      ),
	      'borders' => array(
	        'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), 
	        'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  
	        'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), 
	        'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) 
	      )
	    );
	    $excel->setActiveSheetIndex(0)->setCellValue('A1', "PURCHASE ORDER"); 
	    $excel->getActiveSheet()->mergeCells('A1:O1'); 
	    $excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); 
	    $excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); 
	    $excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1
	   
	    $excel->setActiveSheetIndex(0)->setCellValue('A3', "PO NO"); 
	    $excel->setActiveSheetIndex(0)->setCellValue('B3', "STATUS"); 
	    $excel->setActiveSheetIndex(0)->setCellValue('C3', "PO DATE"); 
	    $excel->setActiveSheetIndex(0)->setCellValue('D3', "VENDOR"); 
	    $excel->setActiveSheetIndex(0)->setCellValue('E3', "DELIVERY TIME"); 
	    $excel->setActiveSheetIndex(0)->setCellValue('F3', "PAYMENT METHOD"); 
	    $excel->setActiveSheetIndex(0)->setCellValue('G3', "PAYMENT TERM"); 
	    $excel->setActiveSheetIndex(0)->setCellValue('H3', "NOTE"); 
	    $excel->setActiveSheetIndex(0)->setCellValue('I3', "DISCOUNT %"); 
	    $excel->setActiveSheetIndex(0)->setCellValue('J3', "VAT %"); 
	    $excel->setActiveSheetIndex(0)->setCellValue('K3', "DOWNPAYMENT"); 
	    $excel->setActiveSheetIndex(0)->setCellValue('L3', "GRAND TOTAL"); 
	    $excel->setActiveSheetIndex(0)->setCellValue('M3', "RECEIVER NAME"); 
	    $excel->setActiveSheetIndex(0)->setCellValue('N3', "PREFERED BY"); 
	    $excel->setActiveSheetIndex(0)->setCellValue('O3', "APPROVED BY"); 

	    $excel->getActiveSheet()->getStyle('A3')->applyFromArray($style_col);
	    $excel->getActiveSheet()->getStyle('B3')->applyFromArray($style_col);
	    $excel->getActiveSheet()->getStyle('C3')->applyFromArray($style_col);
	    $excel->getActiveSheet()->getStyle('D3')->applyFromArray($style_col);
	    $excel->getActiveSheet()->getStyle('E3')->applyFromArray($style_col);
	    $excel->getActiveSheet()->getStyle('F3')->applyFromArray($style_col);
	    $excel->getActiveSheet()->getStyle('G3')->applyFromArray($style_col);
	    $excel->getActiveSheet()->getStyle('H3')->applyFromArray($style_col);
	    $excel->getActiveSheet()->getStyle('I3')->applyFromArray($style_col);
	    $excel->getActiveSheet()->getStyle('J3')->applyFromArray($style_col);
	    $excel->getActiveSheet()->getStyle('K3')->applyFromArray($style_col);
	    $excel->getActiveSheet()->getStyle('L3')->applyFromArray($style_col);
	    $excel->getActiveSheet()->getStyle('M3')->applyFromArray($style_col);
	    $excel->getActiveSheet()->getStyle('N3')->applyFromArray($style_col);
	    $excel->getActiveSheet()->getStyle('O3')->applyFromArray($style_col);

	    
	    $po = $this->purchase_order_model->get();
	    $no = 1; 
	    $numrow = 4; 
	    foreach($po as $rows){ 
	      $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, $rows['po_no']);
	      $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, getStatusPO($rows['status'], true));
	      $excel->setActiveSheetIndex(0)->setCellValue('C'.$numrow, ConvertDateToString($rows['po_date'],1,1));
	      $excel->setActiveSheetIndex(0)->setCellValue('D'.$numrow, $rows['vendor_name']);
	      $excel->setActiveSheetIndex(0)->setCellValue('E'.$numrow, ConvertDateToString($rows['delivery_time'],1,1));
	      $excel->setActiveSheetIndex(0)->setCellValue('F'.$numrow, $rows['payment_method']);
	      $excel->setActiveSheetIndex(0)->setCellValue('G'.$numrow, $rows['payment_term']);
	      $excel->setActiveSheetIndex(0)->setCellValue('H'.$numrow, $rows['note']);
	      $excel->setActiveSheetIndex(0)->setCellValue('I'.$numrow, $rows['discount']);
	      $excel->setActiveSheetIndex(0)->setCellValue('J'.$numrow, $rows['vat']);
	      $excel->setActiveSheetIndex(0)->setCellValue('K'.$numrow, $rows['downpayment']);
	      $excel->setActiveSheetIndex(0)->setCellValue('L'.$numrow, rp($rows['grand_total']));
	      $excel->setActiveSheetIndex(0)->setCellValue('M'.$numrow, $rows['receiver_name']);
	      $excel->setActiveSheetIndex(0)->setCellValue('N'.$numrow, $rows['prefered_by']);
	      $excel->setActiveSheetIndex(0)->setCellValue('O'.$numrow, $rows['approved_by']);
	      
	      $excel->getActiveSheet()->getStyle('A'.$numrow)->applyFromArray($style_row);
	      $excel->getActiveSheet()->getStyle('B'.$numrow)->applyFromArray($style_row);
	      $excel->getActiveSheet()->getStyle('C'.$numrow)->applyFromArray($style_row);
	      $excel->getActiveSheet()->getStyle('D'.$numrow)->applyFromArray($style_row);
	      $excel->getActiveSheet()->getStyle('E'.$numrow)->applyFromArray($style_row);
	      $excel->getActiveSheet()->getStyle('F'.$numrow)->applyFromArray($style_row);
	      $excel->getActiveSheet()->getStyle('G'.$numrow)->applyFromArray($style_row);
	      $excel->getActiveSheet()->getStyle('H'.$numrow)->applyFromArray($style_row);
	      $excel->getActiveSheet()->getStyle('I'.$numrow)->applyFromArray($style_row);
	      $excel->getActiveSheet()->getStyle('J'.$numrow)->applyFromArray($style_row);
	      $excel->getActiveSheet()->getStyle('K'.$numrow)->applyFromArray($style_row);
	      $excel->getActiveSheet()->getStyle('L'.$numrow)->applyFromArray($style_row);
	      $excel->getActiveSheet()->getStyle('M'.$numrow)->applyFromArray($style_row);
	      $excel->getActiveSheet()->getStyle('N'.$numrow)->applyFromArray($style_row);
	      $excel->getActiveSheet()->getStyle('O'.$numrow)->applyFromArray($style_row);
	      
	      $no++;
	      $numrow++;
	    }
	    
	    $excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
	    $excel->getActiveSheet()->getColumnDimension('B')->setWidth(25); 
	    $excel->getActiveSheet()->getColumnDimension('C')->setWidth(35); 
	    $excel->getActiveSheet()->getColumnDimension('D')->setWidth(30); 
	    $excel->getActiveSheet()->getColumnDimension('E')->setWidth(20); 
	    $excel->getActiveSheet()->getColumnDimension('F')->setWidth(20); 
	    $excel->getActiveSheet()->getColumnDimension('G')->setWidth(60); 
	    $excel->getActiveSheet()->getColumnDimension('H')->setWidth(15); 
	    $excel->getActiveSheet()->getColumnDimension('I')->setWidth(15); 
	    $excel->getActiveSheet()->getColumnDimension('J')->setWidth(15); 
	    $excel->getActiveSheet()->getColumnDimension('K')->setWidth(20); 
	    $excel->getActiveSheet()->getColumnDimension('L')->setWidth(20); 
	    $excel->getActiveSheet()->getColumnDimension('M')->setWidth(20); 
	    $excel->getActiveSheet()->getColumnDimension('N')->setWidth(20); 
	    $excel->getActiveSheet()->getColumnDimension('O')->setWidth(20); 

	    $excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
	    
	    $excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
	    
	    $excel->getActiveSheet(0)->setTitle("Purchase Order");
	    $excel->setActiveSheetIndex(0);
	    
	    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); 
	    header('Content-Disposition: attachment; filename="Purchase-Order-'. date('Y-m-d-H:i:s') .'.xlsx"'); 
	    header('Cache-Control: max-age=0');
	    $write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
	    $write->save('php://output');
	}

}
