<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * 			
 */
class Vendor_master_data extends CI_Controller
{
	
	public $module = "mr_vendor";

	function __construct()
	{
		parent::__construct();

		$this->load->model('Vendor_model');	
	}

	public function index()
	{
		// declare meta

		$data['title'] = "Admin - " . $this->module;
		$data['module_name'] = $this->module;

		// load data

		$data['rows_data'] = $this->Vendor_model->get(array('showAll' => TRUE));

		// load layout

		$this->load->view('common/meta', $data);
		$this->load->view('common/header', $data);
		$this->load->view('common/sidebar');
		$this->load->view('modules/' . $this->module . '/index', $data);
		$this->load->view('common/footer', $data);
	}

	public function add()
	{
		// declare meta

		$data['title'] = "Admin - " . $this->module;
		$data['module_name'] = $this->module;

		// load data

		$data['vendors'] = $this->Vendor_model->get();

		// load layout

		$this->load->view('common/meta', $data);
		$this->load->view('common/header', $data);
		$this->load->view('common/sidebar');
		$this->load->view('modules/' . $this->module . '/form', $data);
		$this->load->view('common/footer', $data);
	}

	public function store()
	{
		// receive post data

		$data = array(
			'vendor_name' => $this->input->post('vendor_name'),
			'address' => $this->input->post('address'),
			'telephone' => $this->input->post('telephone'),
			'pic_name' => $this->input->post('pic_name'),
			'handphone' => $this->input->post('handphone'),
			'fax' => $this->input->post('fax'),
			'email' => $this->input->post('email'),
			'is_active' => $this->input->post('is_active') == null ? '0' : '1', 
			'created_at' => date('Y-m-d H:i:s')
		);


		// store to model
		$this->Vendor_model->store($data);
		redirect('module/' . $this->uri->segment('2'));

	}

	public function edit($id)
	{
		$checking = $this->Vendor_model->get_count('id_vendor',$id);

		if ($checking > 0) {
			$data['id'] = $id;

			// load data

			$data['data'] = $this->Vendor_model->get_row($id); 

			// declare meta

			$data['title'] = "Admin Edit PO - " . $this->module;
			$data['module_name'] = $this->module; 

			// load layout

			$this->load->view('common/meta', $data);
			$this->load->view('common/header', $data);
			$this->load->view('common/sidebar');
			$this->load->view('modules/' . $this->module . '/form', $data);
			$this->load->view('common/footer', $data);

		}else{
			redirect('module/' . $this->uri->segment('2'));
		}

	}

	public function update($id)
	{
		// receive post data

		$data = array(
			'vendor_name' => $this->input->post('vendor_name'),
			'address' => $this->input->post('address'),
			'telephone' => $this->input->post('telephone'),
			'pic_name' => $this->input->post('pic_name'),
			'handphone' => $this->input->post('handphone'),
			'fax' => $this->input->post('fax'),
			'email' => $this->input->post('email'),
			'is_active' => $this->input->post('is_active') == null ? '0' : '1', 
			'updated_at' => date('Y-m-d H:i:s')
		);

		// return pre($data);

		// update to model
		$this->Vendor_model->update($id,$data);
		redirect('module/' . $this->uri->segment('2'));

	}

	public function delete()
	{  
		if (null !== $this->input->post('id')) {
			$this->Vendor_model->delete($this->input->post('id'));
			echo "200";
		}else{
			echo "400";
		}
	}
	
}

 ?>