<?php 
class Bill_of_lading_model extends CI_Model {

        function __construct()
        {
        	$this->table = "bill_of_lading";
        }
        public function get()
        {

                $this->db->select('*');
                $this->db->from($this->table); 
                $this->db->where($this->table.'.is_deleted', 0);
                $query = $this->db->get();

                return $query->result_array();
        } 

        public function get_row($id)
        {

                $this->db->select('*');
                $this->db->from($this->table);
                $this->db->join('purchase_order', $this->table . '.id_po = purchase_order.id_po', 'left');
                $this->db->where($this->table . '.id_bl', $id);
                $query = $this->db->get();

                return $query->row_array();
        }

        public function get_count($param = array())
        {
                if (isset($param['no_bill'])) {
                    $this->db->where('no_bl', $param['no_bill']);
                } 
                
                $this->db->from($this->table);
                $query = $this->db->count_all_results();

                return $query;
        }

        public function store($data)
        {
                $this->db->insert($this->table, $data);
        }

        public function get_bl_items($where,$value)
        {
                $this->db->where($where, $value);
                $this->db->where('bill_of_lading_container.is_deleted', 0);
                $this->db->join('purchase_order_items', 'purchase_order_items.id_po_items = bill_of_lading_container.id_po_item');
                $query = $this->db->get('bill_of_lading_container');

                return $query->result_array();
        }

        public function getTotalBL($id_bl)
        {
                $this->db->select_sum('total');
                $this->db->where('id_bl', $id_bl);
                $this->db->where('is_deleted', 0);
                $query = $this->db->get('bill_of_lading_container')->row_array();

                return $query['total'];
        }

        public function get_item_row($id)
        {

                $this->db->select('*');
                $this->db->from('bill_of_lading_container');
                $this->db->where('id_bl_container', $id);
                $query = $this->db->get();

                return $query->row_array();
        } 

        public function update($id, $data)
        {
                $this->db->where('id_bl', $id);
                $this->db->update($this->table, $data);
        }

        public function delete($id)
        {
               // soft delete
                $this->db->set('is_deleted', '1');
                $this->db->where('id_bl', $id);
                $this->db->update($this->table);
        }

        public function store_item($data)
        {
                $this->db->insert('bill_of_lading_container', $data); 
        }

        public function delete_item($id)
        {
                // soft delete
                $this->db->set('is_deleted', '1');
                $this->db->where('id_bl_container', $id);
                $this->db->update('bill_of_lading_container'); 
        }

        public function update_item($id, $data, $id_bl)
        {
                $this->db->where('id_bl_container', $id);
                $this->db->update('bill_of_lading_container', $data); 
        }

}
 ?>