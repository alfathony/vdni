<?php

class Purchase_order_model extends CI_Model {

        public function get()
        {

                $this->db->select('*');
                $this->db->from('purchase_order');
                $this->db->join('mr_vendor', 'purchase_order.id_vendor = mr_vendor.id_vendor', 'left');
                $this->db->where('purchase_order.is_deleted', 0);
                $query = $this->db->get();

                return $query->result_array();
        } 

        public function get_row($id)
        {

                $this->db->select('*');
                $this->db->from('purchase_order');
                $this->db->join('mr_vendor', 'purchase_order.id_vendor = mr_vendor.id_vendor', 'left');
                $this->db->where('purchase_order.id_po', $id);
                $query = $this->db->get();

                return $query->row_array();
        } 

        public function get_count($param = array())
        {
                if (isset($param['id_po'])) {
                    $this->db->where('id_po', $param['id_po']);
                }

                if (isset($param['status'])) {
                    $this->db->where('status', $param['status']);
                }
                
                $this->db->from('purchase_order');
                $query = $this->db->count_all_results();

                return $query;
        }

        public function store($data)
        {
                $this->db->insert('purchase_order', $data);
        }

        public function update($id, $data)
        {
                $this->db->where('id_po', $id);
                $this->db->update('purchase_order', $data);
        }

        public function delete($id)
        {
               // soft delete
                $this->db->set('is_deleted', '1');
                $this->db->where('id_po', $id);
                $this->db->update('purchase_order');
        }

        public function changestatus($id,$status)
        {
               // soft delete
                $this->db->set('status', $status);
                $this->db->where('id_po', $id);
                $this->db->update('purchase_order');
        }


        public function get_po_items($where,$value)
        { 
                $this->db->where($where, $value);
                $this->db->where('is_deleted', 0);
                $query = $this->db->get('purchase_order_items');

                return $query->result_array();
        }

        public function get_item_row($id)
        {

                $this->db->select('*');
                $this->db->from('purchase_order_items');
                $this->db->where('id_po_items', $id);
                $query = $this->db->get();

                return $query->row_array();
        }

        public function store_item($data)
        {
                $this->db->insert('purchase_order_items', $data);

                // update grand total
                $getTotalPO = $this->getTotalPO($data['id_po']);
                $this->update($data['id_po'], array('grand_total' => $getTotalPO));
        }

        public function delete_item($id)
        {
                // soft delete
                $this->db->set('is_deleted', '1');
                $this->db->where('id_po_items', $id);
                $this->db->update('purchase_order_items');

                // update grand total
                $getIdPO = $this->get_item_row($id);

                $getTotalPO = $this->getTotalPO($getIdPO['id_po']);
                $this->update($getIdPO['id_po'], array('grand_total' => $getTotalPO));
        }

        public function update_item($id, $data, $id_po)
        {
                $this->db->where('id_po_items', $id);
                $this->db->update('purchase_order_items', $data);

                // update grand total
                $getTotalPO = $this->getTotalPO($id_po);
                $this->update($id_po, array('grand_total' => $getTotalPO));
        }

        public function getTotalPO($id_po)
        {
                $this->db->select_sum('total');
                $this->db->where('id_po', $id_po);
                $this->db->where('is_deleted', 0);
                $query = $this->db->get('purchase_order_items')->row_array();

                return $query['total'];
        }

}