<?php

/**
 * @author Alfatony
 */

class Vendor_model extends CI_Model
{
    /**
     * Multiple array parrams
     * @param showAll = show all row active and non active unless deleted
     */
	
	public function get($param = array())
	{
        $this->db->where('is_deleted', '0');

        if (isset($param['showAll']) && $param['showAll'] == TRUE) {

        }else{
            $this->db->where('is_active', '1');
        }

        $this->db->order_by('vendor_name', 'asc');
		$query = $this->db->get('mr_vendor');

        return $query->result_array();
	}

	public function get_row($id)
    {

            $this->db->select('*');
            $this->db->from('mr_vendor'); 
            $this->db->where('id_vendor', $id);
            $query = $this->db->get();

            return $query->row_array();
    } 

    public function get_count($where,$value)
    {
            $this->db->where($where, $value);
            $this->db->from('mr_vendor');
            $query = $this->db->count_all_results();

            return $query;
    }

    public function store($data)
    { 
            $this->db->insert('mr_vendor', $data);
    }

    public function update($id, $data)
    {
            $this->db->where('id_vendor', $id);
            $this->db->update('mr_vendor', $data);
    }

    public function delete($id)
    {
           // soft delete
            $this->db->set('is_deleted', '1');
            $this->db->where('id_vendor', $id);
            $this->db->update('mr_vendor');
    }

}

?>