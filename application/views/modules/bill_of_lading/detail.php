<section class="content">
     <div class="container-fluid">
            <div class="block-header pull-right">

                 
                
            </div>

            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-xs-12">
                    <div class="card">
                        <div class="header">
                            <div class="row clearfix">
                                <div class="col-xs-12 col-sm-6">
                                    <h2>
                                        Bill of Lading
                                        
                                        <small>Detail data of bill of lading <?php echo (null !== $data['updated_at']) ? "updated at " .  time_since2($data['updated_at']) : "" ?></small>
                                    </h2>
                                </div>
                                <div class="col-xs-12 col-sm-6 align-right icon-and-text-button-demo">

                                    <ul class="header-dropdown m-r--5">
                                        <li class="dropdown">
                                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                                <i class="material-icons">more_vert</i>
                                            </a>
                                            <ul class="dropdown-menu pull-right">
                                                <li><a href="../edit/<?php echo $data['id_bl'] ?>">Edit BL</a></li>
                                                <!-- <li>
                                                    <a href="javascript:void(0);"
                                                        class="act-change-status"
                                                        data-id="<?php echo $data['id_bl']; ?>"
                                                        data-status="4"
                                                        data-title="Cancel PO"
                                                        data-desc=""
                                                        >Cancel BL</a></li> -->
                                                <li style="border-top : 1px solid #eee">
                                                    <a href="javascript:void(0);" class="text-danger do-delete" data-id="<?php echo $data['id_bl']; ?>" data-name="<?php echo generatePoCode($data['po_no'],$data['po_date']); ?>"><i class="material-icons">delete</i> Archive</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="body">

                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="">
                                        <div class="row">
                                            <div class="col-md-4 text-right"><strong>Related to PO</strong></div>
                                            <div class="col-md-8"><a href="../../purchase-order/detail/<?php echo $data['id_po'] ?>"><?php echo $data['po_no'] ?></a></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 text-right"><strong>Bill of Lading NO</strong></div>
                                            <div class="col-md-8"><?php echo $data['no_bl'] ?></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 text-right"><strong>Date</strong></div>
                                            <div class="col-md-8"><?php echo ConvertDateToString($data['date'],1,1) ?></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 text-right"><strong>Shipper</strong></div>
                                            <div class="col-md-8"><?php echo $data['shipper_name'] ?></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 text-right"><strong>Consignee</strong></div>
                                            <div class="col-md-8"><?php echo $data['consignee']; ?></div>
                                        </div> 
                                    </div>
                                </div>  

                                <div class="col-md-6 col-sm-6 col-xs-12 pull-right">
                                    <div class="">
                                        <div class="row">
                                            <div class="col-md-4 text-right"><strong>Vessel/ Voyage No</strong></div>
                                            <div class="col-md-8"><?php echo $data['vessel'] ?> / <?php echo $data['voyage_no'] ?></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 text-right"><strong>Port of Loading</strong></div>
                                            <div class="col-md-8"><?php echo $data['port_of_loading'] ?></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 text-right"><strong>Port of Discharge</strong></div>
                                            <div class="col-md-8"><?php echo $data['port_of_discharge'] ?></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 text-right"><strong>Notify Party</strong></div>
                                            <div class="col-md-8"><?php echo $data['notify_party'] ?></div>
                                        </div>
                                    </div>
                                </div>  

                                
                            </div>

                        	<?php $this->load->view('modules/'. $module_name .'/item_list', $rows_data); ?> 

                        <hr>

                        <div class="row">
                            <div class="col-md-4 text-center">
                                <strong>Number of Original Bill(s) of Lading</strong> <br>
                                <?php echo $data['no_original_bl'] ?><br>
                            </div>
                            <div class="col-md-4 text-center">
                            </div>
                            <div class="col-md-4 text-center">
                                <strong>Master's Name</strong> <br>
                                <?php echo $data['master_name'] ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
            
	</div>
</section>