<?php 

if (isset($id) && $id != null){
    $title = "EDIT BILL OF LADING";
    $subtitle = "Edit this bill of lading data";
    $action = "/update/" . $id;
}else{
    $title = "ADD BILL OF LADING";
    $subtitle = "Add new bill of lading data";
    $action = "/store";
}

?>

<section class="content">
     <div class="container-fluid">


            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <div class="clearfix">
                                <div class="col-xs-12 col-sm-6">
                                    <h2>
                                        <?php echo $title; ?>
                                        <small><?php echo $subtitle; ?></small>
                                    </h2>
                                </div>
                                <div class="col-xs-12 col-sm-6 align-right">
                                </div>
                            </div>
                        </div>
                        <div class="body">
                            <?php echo form_open('module/' . $this->uri->segment(2) . $action); ?>

                                <div class="row clearfix">
                                    <div class="col-md-12">
                                        <label for="email_address">No Bill of Lading</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" value="<?php echo isset($data['no_bl']) ? $data['no_bl'] : ''; ?>" placeholder="" name="no_bill_of_lading" required="">
                                            </div>
                                        </div>
                                    </div> 

                                    <div class="col-md-12">
                                        <label for="email_address">Related to PO</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <?php  
                                                    $option = array();
                                                    $option[0] = " - Select Related to PO -";

                                                    foreach ($purchase_orders as $value){ $option[$value['id_po']] = $value['po_no']; }

                                                    echo form_dropdown('id_po', $option, isset($data['id_po']) ? $data['id_po'] : '', "class='form-control show-tick'"); 

                                                    ?>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <label for="email_address">Shipper Name</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" value="<?php echo isset($data['shipper_name']) ? $data['shipper_name'] : ''; ?>" placeholder="" name="shipper_name">
                                            </div>
                                        </div>
                                    </div> 

                                    <div class="col-md-6">
                                        <label for="email_address">Consignee</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" value="<?php echo isset($data['consignee']) ? $data['consignee'] : ''; ?>" placeholder="" name="consignee">
                                            </div>
                                        </div>
                                    </div>  

                                    <div class="col-md-12">
                                        <label for="email_address">Date</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="date" class="form-control" value="<?php echo isset($data['date']) ? $data['date'] : ''; ?>" placeholder="" name="date">
                                            </div>
                                        </div>
                                    </div>       

                                    <div class="col-md-6">
                                        <label for="email_address">Vessel</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" value="<?php echo isset($data['vessel']) ? $data['vessel'] : ''; ?>" placeholder="" name="vessel">
                                            </div>
                                        </div>
                                    </div>    

                                    <div class="col-md-6">
                                        <label for="email_address">No Voyage</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" value="<?php echo isset($data['voyage_no']) ? $data['voyage_no'] : ''; ?>" placeholder="" name="no_voyage">
                                            </div>
                                        </div>
                                    </div>       

                                    <div class="col-md-6">
                                        <label for="email_address">Port of Loading</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" value="<?php echo isset($data['port_of_loading']) ? $data['port_of_loading'] : ''; ?>" placeholder="" name="port_of_loading">
                                            </div>
                                        </div>
                                    </div>            

                                    <div class="col-md-6">
                                        <label for="email_address">Port of Discharge</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" value="<?php echo isset($data['port_of_discharge']) ? $data['port_of_discharge'] : ''; ?>" placeholder="" name="port_of_discharge">
                                            </div>
                                        </div>
                                    </div>      

                                    <div class="col-md-4">
                                        <label for="email_address">Notify Party</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" value="<?php echo isset($data['notify_party']) ? $data['notify_party'] : ''; ?>" placeholder="" name="notify_party">
                                            </div>
                                        </div>
                                    </div>     

                                    <div class="col-md-4">
                                        <label for="email_address">No Original Bill of Lading</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" value="<?php echo isset($data['no_original_bl']) ? $data['no_original_bl'] : ''; ?>" placeholder="" name="no_original_bill_of_lading">
                                            </div>
                                        </div>
                                    </div>      

                                    <div class="col-md-4">
                                        <label for="email_address">Master Name</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" value="<?php echo isset($data['master_name']) ? $data['master_name'] : ''; ?>" placeholder="" name="master_name">
                                            </div>
                                        </div>
                                    </div>       
                                     
                                </div>

                                <div class="clearfix">
                                    <div class="col-xs-12 col-sm-6">
                                    </div>
                                    <div class="col-xs-12 col-sm-6 align-right">
                                        <input type="submit" name="submit" value="SAVE BILL OF LADING" class="btn btn-success btn-lg waves-effect">
                                    </div>
                                </div>
                                
                            </form>
                        </div>

                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
            
	</div>
</section>