<section class="content">
     <div class="container-fluid">


            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <div class="clearfix">
                                <div class="col-xs-12 col-sm-6">
                                    <h2>
                                        BILL OF LADING
                                        <small>List data of bill of lading</small>
                                    </h2>
                                </div> 
                                <div class="col-xs-12 col-sm-6 align-right"> 
                                    <a href="<?php echo current_url() ?>/add" class="btn btn-primary btn-lg waves-effect">Add bill Of Lading</a>
                                </div>
                            </div>
                            </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable table-po">
                                    <thead>
                                        <tr>
                                            <th>Date</th>
                                            <th>Shipper</th>
                                            <th>Vessel</th>
                                            <th>Port</th> 
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($rows_data as $rows): ?>
                                            <tr class="record">
                                                <td>
                                                    <?php echo convertDateToString($rows['date'],1,1); ?>
                                                </td>
                                                <td>
                                                    <?php echo $rows['shipper_name']; ?>
                                                </td> 
                                                <td>
                                                    <?php echo $rows['vessel']; ?>
                                                </td>
                                                <td>
                                                    Loading : <?php echo $rows['port_of_loading']; ?> <br>
                                                    Discharge : <?php echo $rows['port_of_discharge']; ?>
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-primary btn-sm waves-effect" data-toggle="tooltip" data-placement="top" title="" data-original-title="View detail" onclick="location.href='<?php echo current_url().'/detail/'.$rows['id_bl']; ?>'"><i class="material-icons">remove_red_eye</i></button>
                                                </td>
                                            </tr>  
                                        <?php endforeach ?>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
            
    </div>
</section>