<section class="content">
     <div class="container-fluid">


            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <div class="clearfix">
                                <div class="col-xs-12 col-sm-6">
                                    <h2>
                                        <?php echo $title; ?>
                                        <small><?php echo $subtitle; ?></small>
                                    </h2>
                                </div>
                                <div class="col-xs-12 col-sm-6 align-right">
                                </div>
                            </div>
                        </div>
                        <div class="body">
                            <?php echo form_open('module/' . $this->uri->segment(2) . $action); ?>
                            <input type="hidden" name="id_bl" value="<?php echo isset($data['id_po']) ? $data['id_bl'] : ''; ?>">
                                
                                <div class="row clearfix">
                                    <div class="col-md-6">
                                        <label for="email_address">Select Item Cargo</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <?php  
                                                    $option = array();
                                                    $option[0] = " - Select Item -";

                                                    foreach ($purchase_items as $value){ $option[$value['id_po_items']] = $value['item']; }

                                                    echo form_dropdown('id_po_item', $option, isset($data['id_po_item']) ? $data['id_po_item'] : '', "class='form-control show-tick' required=''"); 

                                                    ?>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>

                                    <div class="col-md-4">
                                        <label for="email_address">Container Number</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" value="<?php echo isset($data['number_container']) ? $data['number_container'] : ''; ?>" placeholder="" name="number_container" required="">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <label for="email_address">Container Type</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" value="<?php echo isset($data['type_container']) ? $data['type_container'] : ''; ?>" placeholder="" name="type_container" required="">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <label for="email_address">Seal</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" value="<?php echo isset($data['seal']) ? $data['seal'] : ''; ?>" placeholder="" name="seal" required="">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <label for="email_address">Ton</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="number" step=0.01  class="form-control" value="<?php echo isset($data['ton']) ? $data['ton'] : ''; ?>" placeholder="" name="ton" required="">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <label for="email_address">Ton Checked</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="number" step=0.01  class="form-control" value="<?php echo isset($data['ton_checked']) ? $data['ton_checked'] : ''; ?>" placeholder="" name="ton_checked" required="">
                                            </div>
                                        </div>
                                    </div> 

                                <div class="clearfix">
                                    <div class="col-xs-12 col-sm-6">
                                    </div>
                                    <div class="col-xs-12 col-sm-6 align-right">
                                        <input type="submit" name="submit" value="SAVE BILL OF LADING CONTAINER" class="btn btn-success btn-lg waves-effect">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
            
	</div>
</section>