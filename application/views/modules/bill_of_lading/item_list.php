<div class="clearfix row">
    <div class="col-xs-12 col-sm-6">
    </div>
    <div class="col-xs-12 col-sm-6 align-right icon-and-text-button-demo">
        <a href="../itemform/<?php echo $data['id_bl'] ?>/add" class="btn btn-default waves-effect"><i class="material-icons">add</i><span>Add Item</span></a>
    </div>
</div>

<div class="table-responsive">
    <table class="table table-bordered table-striped table-hover dataTable js-basic-example">
        <thead>
            <tr>
                <th rowspan="2" style="text-align: center;vertical-align: middle;">No</th>
                <th rowspan="2" style="text-align: left;vertical-align: middle;">Cargo</th>
                <th colspan="2" style="text-align: center;vertical-align: middle;">Container</th>
                <th rowspan="2" style="text-align: center;vertical-align: middle;">Seal</th> 
                <th rowspan="2" style="text-align: center;vertical-align: middle;">Ton</th>  
                <th rowspan="2" style="text-align: center;vertical-align: middle;">Ton Checked</th>  
                <th rowspan="2" style="text-align: center;vertical-align: middle;">Action</th>  
            </tr>
            <tr>
                <th style="text-align: center;vertical-align: middle;">Number</th>
                <th style="text-align: center;vertical-align: middle; border-right-width:1px">Type</th> 
            </tr>
        </thead>
        <tbody>
            <?php 
            if (count($rows_data) == 0) {
                ?>
                <tr>
                    <td class="text-center" colspan="8">No Data Bill of Lading.</td>
                </tr>
                <?php
            }
             ?>
            <?php $no = 1; ?>
            <?php $checkExcess = null; ?>
            <?php $checkFit = null; ?>
            <?php foreach ($rows_data as $rows): ?>
            <?php $checkExcess = $rows['ton_checked'] > $rows['ton'] ? "+ ": ""; ?>
            <?php $checkFit = $rows['ton_checked'] - $rows['ton']; ?>
                <tr>
                    <td class="text-center"><?php echo $no; ?></td>
                    <td><?php echo $rows['item']; ?></td>
                    <td class="text-center"><?php echo $rows['number_container']; ?></td>
                    <td class="text-center"><?php echo $rows['type_container']; ?></td>
                    <td class="text-center"><?php echo $rows['seal']; ?></td> 
                    <td class="text-center"><?php echo $rows['ton']; ?></td> 
                    <td class="text-center"><?php echo $rows['ton_checked']; ?> (<?php echo $checkFit == 0 ? "Fitted" : $checkExcess.$checkFit ?>)</td> 
                    <td class="text-center">
                        <a 
                            href="<?php echo '../itemform/'.$data['id_bl'].'/edit/'.$rows['id_bl_container']; ?>" 
                            style="padding:10px" 
                            data-toggle="tooltip" 
                            data-placement="top" 
                            title="" 
                            data-original-title="Edit item">
                                <i class="material-icons">mode_edit</i></a>

                        <a 
                            href="<?php echo '../delete_item/'.$rows['id_bl_container']; ?>" 
                            style="padding:10px" 
                            data-toggle="tooltip" 
                            data-placement="top" 
                            title="" 
                            data-original-title="Delete item" 
                            onclick="return confirm('Are you sure want to delete this?');">
                                <i class="material-icons text-danger">delete</i></a>
                    </td>
                </tr>  
                <?php $no++; ?>
            <?php endforeach ?>
            
        </tbody>
    </table>
</div>