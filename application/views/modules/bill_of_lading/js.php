    <!-- Jquery Core Js -->
    <script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
    <script src="<?php echo base_url(); ?>assets/plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="<?php echo base_url(); ?>assets/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?php echo base_url(); ?>assets/plugins/node-waves/waves.js"></script>

    <!-- Jquery DataTable Plugin Js -->
    <script src="<?php echo base_url(); ?>assets/plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

    <!-- Moment Plugin Js -->
    <script src="<?php echo base_url(); ?>assets/plugins/momentjs/moment.js"></script>

    <!-- Input Mask Plugin Js -->
    <script src="<?php echo base_url(); ?>assets/plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script>
    <!-- <script src="<?php echo base_url(); ?>assets/plugins/jquery-masking/jquery.mask.min.js"></script> -->

    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>

    <!-- SweetAlert Plugin Js -->
    <script src="<?php echo base_url(); ?>assets/plugins/sweetalert/sweetalert.min.js"></script>

    <!-- Custom Js -->
    <script src="<?php echo base_url(); ?>assets/js/admin.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/pages/forms/basic-form-elements.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/pages/ui/tooltips-popovers.js"></script>

    <script type="text/javascript">
        $(document).ready(function(){
            // input mask rupiah
            
            $('.format-rupiah').inputmask('Rp 99.999.999.999.999', {
                numericInput: true, 
                removeMaskOnSubmit: true,
                rightAlign: false
            });
        });

        $(function () {
            
            

            // data table po
            $('.table-po').DataTable({
                "responsive": true,
                "order": [[ 1, "asc" ]],
                "columnDefs": [
                    { "orderable": false, "targets": 5 }
                  ]
            });

            // delete po
            $('.do-delete').on('click', function () {

                var name = $(this).data('name');
                var id = $(this).data('id');
                var currentLocation = window.location.href;

                swal({
                  title: "Archive PO " + name + "?",
                  text: "Once archive, you will not be able to use this data!",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                })
                .then((willDelete) => {
                  if (willDelete) {

                    // ajax delete
                    $.ajax({
                         type: "POST",
                         url : '../delete',
                         data: {id:id},
                         success: function(data){
                            console.log(data + ' success');
                        }
                    });

                    swal("PO " + name + " has been archived!", {
                        icon: "success",
                    });

                    window.location.href='../';
                    
                  } else {
                    swal("Your data is safe!");
                  }
                });


            });


            // cancel po
            $('.act-change-status').on('click', function () {

                var title = $(this).data('title');
                var desc = $(this).data('desc');
                var status = $(this).data('status');
                var id = $(this).data('id');

                swal({
                  title: "Do you want to " + title + "?",
                  text: desc,
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                })
                .then((willDelete) => {
                  if (willDelete) {

                    // ajax delete
                    $.ajax({
                         type: "POST",
                         url : '../changestatus',
                         data: {id:id, status:status},
                         success: function(data){
                            console.log(data + ' success');
                        }
                    });

                    swal("Status has been changed", {
                        icon: "success",
                    });

                    window.location.href='../';
                    
                  }
                });


            });
        });
    </script>

    <!-- Demo Js -->
    <script src="<?php echo base_url(); ?>assets/js/demo.js"></script>