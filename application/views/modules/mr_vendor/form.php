<?php 

if (isset($id) && $id != null){
    $title = "EDIT VENDOR MASTER DATA";
    $subtitle = "Edit this vendor master data";
    $action = "/update/" . $id;
}else{
    $title = "ADD VENDOR MASTER DATA";
    $subtitle = "Add new vendor master data";
    $action = "/store";
}

?>

<section class="content">
     <div class="container-fluid">


            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <div class="clearfix">
                                <div class="col-xs-12 col-sm-6">
                                    <h2>
                                        <?php echo $title; ?>
                                        <small><?php echo $subtitle; ?></small>
                                    </h2>
                                </div>
                                <div class="col-xs-12 col-sm-6 align-right">
                                </div>
                            </div>
                        </div>
                        <div class="body">
                            <?php echo form_open('module/' . $this->uri->segment(2) . $action); ?>
                                <div class="row clearfix">
                                    <div class="col-md-12">
                                        <label for="email_address">Vendor Name</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" value="<?php echo isset($data['vendor_name']) ? $data['vendor_name'] : ''; ?>" placeholder="" name="vendor_name" required="">
                                            </div>
                                        </div>
                                    </div> 

                                    <div class="col-md-12">
                                        <label for="email_address">Address</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <textarea rows="4" class="form-control no-resize" placeholder="" name="address"><?php echo isset($data['address']) ? $data['address'] : ''; ?></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <label for="email_address">Telephone</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" value="<?php echo isset($data['telephone']) ? $data['telephone'] : ''; ?>" placeholder="" name="telephone">
                                            </div>
                                        </div>
                                    </div> 

                                    <div class="col-md-4">
                                        <label for="email_address">Fax</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" value="<?php echo isset($data['fax']) ? $data['fax'] : ''; ?>" placeholder="" name="fax">
                                            </div>
                                        </div>
                                    </div> 

                                     <div class="col-md-4">
                                        <label for="email_address">Handphone</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" value="<?php echo isset($data['handphone']) ? $data['handphone'] : ''; ?>" placeholder="" name="handphone">
                                            </div>
                                        </div>
                                    </div> 

                                    <div class="col-md-6">
                                        <label for="email_address">Pic Name</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" value="<?php echo isset($data['pic_name']) ? $data['pic_name'] : ''; ?>" placeholder="" name="pic_name">
                                            </div>
                                        </div>
                                    </div>  

                                    <div class="col-md-6">
                                        <label for="email_address">Email</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="email" class="form-control" value="<?php echo isset($data['email']) ? $data['email'] : ''; ?>" placeholder="" name="email">
                                            </div>
                                        </div>
                                    </div> 

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <?php 
                                                $data = array(
                                                        'name'          => 'is_active',
                                                        'value'         => '1',
                                                        'checked'       => isset($data['is_active']) && $data['is_active'] == 1 ? TRUE : FALSE,
                                                        'class'         => 'filled-in',
                                                        'id'            => 'basic_checkbox_2',
                                                );

                                                echo form_checkbox($data); 

                                                ?>
                                                <label for="basic_checkbox_2">Active vendor</label> 
                                            </div>
                                        </div>
                                    </div> 
                                     
                                </div>

                                <div class="clearfix">
                                    <div class="col-xs-12 col-sm-6">
                                    </div>
                                    <div class="col-xs-12 col-sm-6 align-right">
                                        <input type="submit" name="submit" value="SAVE VENDOR" class="btn btn-success btn-lg waves-effect">
                                    </div>
                                </div>
                                
                            </form>
                        </div>

                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
            
	</div>
</section>