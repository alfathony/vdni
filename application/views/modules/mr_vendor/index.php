<section class="content">
     <div class="container-fluid">


            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <div class="clearfix">
                                <div class="col-xs-12 col-sm-6">
                                    <h2>
                                        VENDOR MASTER DATA
                                        <small>List data of vendor master data</small>
                                    </h2>
                                </div>
                                <div class="col-xs-12 col-sm-6 align-right">
                                    <a href="<?php echo current_url() ?>/add" class="btn btn-primary btn-lg waves-effect">Add Vendor Master Data</a>
                                </div>
                            </div>
                            </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable table-vendor">
                                    <thead>
                                        <tr>
                                            <th>Vendor Name</th>
                                            <th>Address</th>
                                            <th>Contact</th> 
                                            <th>Pic Name</th>
                                            <th>Email</th>
                                            <th>Active</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	<?php foreach ($rows_data as $rows): ?>
                                    		<tr class="record">
                                                <td> 
                                                    <?php echo $rows['vendor_name']; ?>
                                                </td>
                                                <td>
                                                	<?php echo $rows['address']; ?>
                                                </td>
                                                <td>
                                                	Telp : <?php echo $rows['telephone']; ?> <br>
                                                	Fax : <?php echo $rows['fax']; ?> <br>
                                                    Hp : <?php echo $rows['handphone']; ?> <br>
                                                </td> 
                                                <td>
                                                    <?php echo $rows['pic_name']; ?>
                                                </td>
                                                <td>
                                                	<?php echo $rows['email']; ?>
                                                </td>
                                                <td>
                                                	<?php echo $rows['is_active'] == '1' ? '<label class="label label-success">Active</label>' : '<label class="label label-danger">Deactive</label>'; ?>
                                                </td> 
	                                            <td>
                                                    <button type="button" class="btn btn-primary btn-sm waves-effect" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit vendor" onclick="window.location='<?php echo current_url().'/edit/'.$rows['id_vendor']; ?>'"><i class="material-icons">edit</i></button> 
                                                    <button type="button" class="btn btn-danger btn-sm waves-effect vendor-delete" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete vendor" data-name="<?php echo $rows['vendor_name']; ?>" data-id="<?php echo $rows['id_vendor']; ?>" data-url="<?php echo current_url().'/delete'; ?>"><i class="material-icons">delete</i></button> 
                                                    <!-- <button type="button" class="btn btn-primary btn-sm waves-effect" data-toggle="tooltip" data-placement="top" title="" data-original-title="View detail" onclick="location.href='<?php echo current_url().'/detail/'.$rows['id_vendor']; ?>'"><i class="material-icons">remove_red_eye</i></button> -->
                                                </td>
                                        	</tr>  
                                    	<?php endforeach ?>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
            
	</div>
</section>

