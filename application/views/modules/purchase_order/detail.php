<section class="content">
     <div class="container-fluid">
            <div class="block-header pull-right">

                <?php if ($data['status'] == 1): ?>
                    <a href="javascript:void(0);"
                    class="btn btn-primary waves-effect act-change-status"
                    data-id="<?php echo $data['id_po']; ?>"
                    data-status="2"
                    data-title="Send Request Approval"
                    data-desc=""><i class="material-icons">description</i><span>Send Request Approval</span></a>
                <?php endif ?>

                <?php if ($data['status'] == 2): ?>
                    <a href="javascript:void(0);"
                    class="btn btn-success waves-effect act-change-status"
                    data-id="<?php echo $data['id_po']; ?>"
                    data-status="3"
                    data-title="Approve PO"
                    data-desc="This PO will be processed immediately if it has been declared approved"><i class="material-icons">done</i><span>Approve PO</span></a>
                <?php endif ?>
                
            </div>

            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-xs-12">
                    <div class="card">
                        <div class="header">
                            <div class="row clearfix">
                                <div class="col-xs-12 col-sm-6">
                                    <h2>
                                        Purchase Order
                                        
                                        <small>Detail data of purchase order <?php echo (null !== $data['updated_at']) ? "updated at " .  time_since2($data['updated_at']) : "" ?></small>
                                    </h2>
                                </div>
                                <div class="col-xs-12 col-sm-6 align-right icon-and-text-button-demo">

                                    <ul class="header-dropdown m-r--5">
                                        <li class="dropdown">
                                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                                <i class="material-icons">more_vert</i>
                                            </a>
                                            <ul class="dropdown-menu pull-right">
                                                <li><a href="../edit/<?php echo $data['id_po'] ?>">Edit PO</a></li>
                                                <li>
                                                    <a href="javascript:void(0);"
                                                        class="act-change-status"
                                                        data-id="<?php echo $data['id_po']; ?>"
                                                        data-status="4"
                                                        data-title="Cancel PO"
                                                        data-desc=""
                                                        >Cancel PO</a></li>
                                                <li style="border-top : 1px solid #eee">
                                                    <a href="javascript:void(0);" class="text-danger do-delete" data-id="<?php echo $data['id_po']; ?>" data-name="<?php echo generatePoCode($data['po_no'],$data['po_date']); ?>"><i class="material-icons">delete</i> Archive</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="body">

                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="">
                                        <div class="row">
                                            <div class="col-md-4 text-right"><strong>PO Number</strong></div>
                                            <div class="col-md-8"><?php echo $data['po_no'] ?></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 text-right"><strong>Status</strong></div>
                                            <div class="col-md-8"><?php echo getStatusPO($data['status']); ?></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 text-right"><strong>PO Date</strong></div>
                                            <div class="col-md-8"><?php echo ConvertDateToString($data['po_date'],1,1); ?></div>
                                        </div>
                                    </div>
                                </div>  

                                <div class="col-md-6 col-sm-6 col-xs-12 pull-right">
                                    <div class="">
                                        <div class="row">
                                            <div class="col-md-4 text-right"><strong>Vendor Name</strong></div>
                                            <div class="col-md-8"><?php echo $data['vendor_name'] ?></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 text-right"><strong>No Handphone</strong></div>
                                            <div class="col-md-8"><?php echo $data['handphone'] ?></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 text-right"><strong>No Telephone</strong></div>
                                            <div class="col-md-8"><?php echo $data['telephone'] ?></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 text-right"><strong>E-Mail</strong></div>
                                            <div class="col-md-8"><?php echo $data['email'] ?></div>
                                        </div>
                                    </div>
                                </div>  

                                

                                <div class="col-sm-4 col-xs-12 hidden">
                                    <div class="panel panel-default">
                                      <div class="panel-heading">
                                        <h3 class="panel-title">Delivery Info</h3>
                                      </div>
                                      <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-4 text-right"><strong>Receiver Name</strong></div>
                                            <div class="col-md-8"><?php echo $data['receiver_name'] ?></div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-4 text-right"><strong>Received Date</strong></div>
                                            <div class="col-md-8"><?php echo ConvertDateToString($data['receiver_date'],1,1) ?></div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-4 text-right"><strong>Prevered By</strong></div>
                                            <div class="col-md-8"><?php echo $data['prefered_by'] ?></div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-4 text-right"><strong>Approved By Total</strong></div>
                                            <div class="col-md-8"><?php echo $data['approved_by'] ?></div>
                                        </div>
                                      </div>
                                    </div>
                                </div>
                            </div>

                        	<?php $this->load->view('modules/'. $module_name .'/item_list', $rows_data); ?>

                            <div class="row" style="margin-top: 48px">

                                <div class="col-sm-6 col-xs-12">
                                        <div class="row">
                                            <div class="col-md-4 text-right"><strong>Delivery Time</strong></div>
                                            <div class="col-md-8"><?php echo ConvertDateToString($data['delivery_time'],1,1) ?></div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-4 text-right"><strong>Payment Method</strong></div>
                                            <div class="col-md-8"><?php echo $data['payment_method'] ?></div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-4 text-right"><strong>Payment Term</strong></div>
                                            <div class="col-md-8"><?php echo $data['payment_term'] ?></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 text-right"><strong>Note</strong></div>
                                            <div class="col-md-8"><?php echo $data['note'] ?></div>
                                        </div>
                                </div>  


                                <div class="col-sm-6 col-xs-12 pull-right">
                                        <div class="row">
                                            <div class="col-md-4 text-right"><strong>Discount</strong></div>
                                            <div class="col-md-8"><?php echo $data['discount'] ?>%</div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-4 text-right"><strong>Vat</strong></div>
                                            <div class="col-md-8"><?php echo $data['vat'] ?>%</div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-4 text-right"><strong>Downpayment</strong></div>
                                            <div class="col-md-8"><?php echo $data['downpayment'] ?>%</div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-4 text-right"><strong>Grand Total</strong></div>
                                            <div class="col-md-8"><?php echo rp($data['grand_total']) ?></div>
                                </div>
                            </div>
                            
                        </div>

                        <hr>

                        <div class="row">
                            <div class="col-md-4 text-center">
                                <strong>Received by</strong> <br>
                                <?php echo $data['receiver_name'] ?><br>
                                <?php echo ConvertDateToString($data['receiver_date'],1,1) ?>
                            </div>
                            <div class="col-md-4 text-center">
                                <strong>Prepared by</strong> <br>
                                <?php echo $data['prefered_by'] ?>
                            </div>
                            <div class="col-md-4 text-center">
                                <strong>Approved by</strong> <br>
                                <?php echo $data['approved_by'] ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
            
	</div>
</section>