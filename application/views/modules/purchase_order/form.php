<?php 

if (isset($id) && $id != null){
    $title = "EDIT PURCHASE ORDER";
    $subtitle = "Edit this purchase order";
    $action = "/update/" . $id;
}else{
    $title = "ADD PURCHASE ORDER";
    $subtitle = "Add new purchase order";
    $action = "/store";
}

?>

<section class="content">
     <div class="container-fluid">


            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <div class="clearfix">
                                <div class="col-xs-12 col-sm-6">
                                    <h2>
                                        <?php echo $title; ?>
                                        <small><?php echo $subtitle; ?></small>
                                    </h2>
                                </div>
                                <div class="col-xs-12 col-sm-6 align-right">
                                </div>
                            </div>
                        </div>
                        <div class="body">
                            <?php echo $this->session->error; ?>
                            <?php echo form_open('module/' . $this->uri->segment(2) . $action); ?>
                                <div class="row clearfix">
                                    <div class="col-md-6">
                                        <label for="email_address">PO Number</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" value="<?php echo isset($data['po_no']) ? $data['po_no'] : ''; ?>" placeholder="" name="po_no" required="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="email_address">PO Date</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="datepicker form-control" value="<?php echo isset($data['po_date']) && $data['po_date'] != '0000-00-00' ? $data['po_date'] : ''; ?>" placeholder="" name="po_date">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <label for="email_address">Vendor</label>

                                        <?php 

                                        $option = array();
                                        $option[0] = " - Select Vendor -";

                                        foreach ($vendors as $value){ $option[$value['id_vendor']] = $value['vendor_name']; }

                                        echo form_dropdown('id_vendor', $option, isset($data['id_vendor']) ? $data['id_vendor'] : '', "class='form-control show-tick'"); 

                                        ?>
                                        
                                    </div>
                                    <div class="col-md-6">
                                        <label for="email_address">Delivery Time</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="datepicker form-control" value="<?php echo isset($data['delivery_time']) && $data['delivery_time'] != '0000-00-00' ? $data['delivery_time'] : ''; ?>" placeholder="" name="delivery_time">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <label for="email_address">Payment Method</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" value="<?php echo isset($data['payment_method']) ? $data['payment_method'] : ''; ?>" placeholder="" name="payment_method">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="email_address">Payment Term</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" value="<?php echo isset($data['payment_term']) ? $data['payment_term'] : ''; ?>" placeholder="" name="payment_term">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <label for="email_address">Note</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <textarea rows="4" class="form-control no-resize" placeholder="Please type what you want..." name="note"><?php echo isset($data['note']) ? $data['note'] : ''; ?></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <label for="email_address">Down Payment %</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" value="<?php echo isset($data['downpayment']) ? $data['downpayment'] : ''; ?>" placeholder="" name="downpayment">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <label for="email_address">Discount %</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" value="<?php echo isset($data['discount']) ? $data['discount'] : ''; ?>" placeholder="" name="discount">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <label for="email_address">VAT %</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" value="<?php echo isset($data['vat']) ? $data['vat'] : ''; ?>" placeholder="" name="vat">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <label for="email_address">Receiver by</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" value="<?php echo isset($data['receiver_name']) ? $data['receiver_name'] : ''; ?>" placeholder="" name="receiver_name">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <label for="email_address">Received Date</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="datepicker form-control" value="<?php echo (isset($data['receiver_date']) && $data['receiver_date'] != '0000-00-00') ? $data['receiver_date'] : ''; ?>" placeholder="" name="receiver_date">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <label for="email_address">Prefered By</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" value="<?php echo isset($data['prefered_by']) ? $data['prefered_by'] : ''; ?>" placeholder="" name="prefered_by">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <label for="email_address">Approved By</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" value="<?php echo isset($data['approved_by']) ? $data['approved_by'] : ''; ?>" placeholder="" name="approved_by">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix">
                                    <div class="col-xs-12 col-sm-6">
                                    </div>
                                    <div class="col-xs-12 col-sm-6 align-right">
                                        <input type="submit" name="submit" value="SAVE PO" class="btn btn-success btn-lg waves-effect">
                                    </div>
                                </div>
                                
                            </form>
                        </div>

                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
            
	</div>
</section>