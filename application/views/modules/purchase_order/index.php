<section class="content">
     <div class="container-fluid">


            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <div class="clearfix">
                                <div class="col-xs-12 col-sm-6">
                                    <h2>
                                        PURCHASE ORDER
                                        <small>List data of purchase order</small>
                                    </h2>
                                </div> 
                                <div class="col-xs-12 col-sm-6 align-right">
                                    <a href="<?php echo current_url() ?>/export" class="btn btn-success btn-lg waves-effect" style="margin-right: 8px;">Export To Excel</a>
                                    <a href="<?php echo current_url() ?>/add" class="btn btn-primary btn-lg waves-effect">Add Purchase Order</a>
                                </div>
                            </div>
                            </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable table-po">
                                    <thead>
                                        <tr>
                                            <th>PO No</th>
                                            <th>PO Date</th>
                                            <th>Vendor</th>
                                            <th>Delivery Time</th>
                                            <th>Grand Total</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($rows_data as $rows): ?>
                                            <tr class="record">
                                                <td><a href="<?php echo current_url().'/detail/'.$rows['id_po']; ?>"><?php echo $rows['po_no']; ?> </a><br>

                                                    <?php echo getStatusPO($rows['status']); ?>
                                                </td>
                                                <td><?php echo ConvertDateToString($rows['po_date'],1,1); ?></td>
                                                <td><?php echo $rows['vendor_name']; ?></td>
                                                <td><?php echo ConvertDateToString($rows['delivery_time'],1,1); ?></td>
                                                <td><?php echo rp($rows['grand_total']); ?></td>
                                                <td>
                                                    <button type="button" class="btn btn-primary btn-sm waves-effect" data-toggle="tooltip" data-placement="top" title="" data-original-title="View detail" onclick="location.href='<?php echo current_url().'/detail/'.$rows['id_po']; ?>'"><i class="material-icons">remove_red_eye</i></button>
                                                </td>
                                            </tr>  
                                        <?php endforeach ?>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
            
    </div>
</section>