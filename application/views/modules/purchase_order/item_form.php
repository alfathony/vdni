<section class="content">
     <div class="container-fluid">


            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <div class="clearfix">
                                <div class="col-xs-12 col-sm-6">
                                    <h2>
                                        <?php echo $title; ?>
                                        <small><?php echo $subtitle; ?></small>
                                    </h2>
                                </div>
                                <div class="col-xs-12 col-sm-6 align-right">
                                </div>
                            </div>
                        </div>
                        <div class="body">
                            <?php echo form_open('module/' . $this->uri->segment(2) . $action); ?>
                            <input type="hidden" name="id_po" value="<?php echo isset($data['id_po']) ? $data['id_po'] : ''; ?>">
                                <div class="row clearfix">
                                    <div class="col-md-12">
                                        <label for="email_address">Item Name</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" value="<?php echo isset($data['item']) ? $data['item'] : ''; ?>" placeholder="" name="item" required="">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <label for="email_address">Item Code</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="number" class="form-control" value="<?php echo isset($data['item_code']) ? $data['item_code'] : ''; ?>" placeholder="" name="item_code" required="">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <label for="email_address">Part No</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="number" class="form-control" value="<?php echo isset($data['part_no']) ? $data['part_no'] : ''; ?>" placeholder="" name="part_no" required="">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <label for="email_address">Quantity</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="number" class="form-control" value="<?php echo isset($data['qty']) ? $data['qty'] : ''; ?>" placeholder="" name="qty" required="">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <label for="email_address">Unit</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" value="<?php echo isset($data['unit']) ? $data['unit'] : ''; ?>" placeholder="" name="unit" required="">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <label for="email_address">Unit Price</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control format-rupiah" value="<?php echo isset($data['unit_price']) ? $data['unit_price'] : ''; ?>" placeholder="" name="unit_price" required="">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <label for="email_address">Discount %</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="number" class="form-control" value="<?php echo isset($data['disc']) ? $data['disc'] : ''; ?>" placeholder="" name="disc">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix">
                                    <div class="col-xs-12 col-sm-6">
                                    </div>
                                    <div class="col-xs-12 col-sm-6 align-right">
                                        <input type="submit" name="submit" value="SAVE ITEM" class="btn btn-success btn-lg waves-effect">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
            
	</div>
</section>