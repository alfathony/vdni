<div class="clearfix row">
    <div class="col-xs-12 col-sm-6">
    </div>
    <div class="col-xs-12 col-sm-6 align-right icon-and-text-button-demo">
        <a href="../itemform/<?php echo $data['id_po'] ?>/add" class="btn btn-default waves-effect"><i class="material-icons">add</i><span>Add Item</span></a>
    </div>
</div>

<div class="table-responsive">
    <table class="table table-bordered table-striped table-hover dataTable js-basic-example">
        <thead>
            <tr>
                <th>No</th>
                <th>Item Code</th>
                <th>Part No</th>
                <th>Item</th>
                <th>Qty</th>
                <th>Unit Price</th>
                <th>Disc</th>
                <th>Amount</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php $no = 1; ?>
            <?php foreach ($rows_data as $rows): ?>
                <tr>
                    <td><?php echo $no; ?></td>
                    <td><?php echo $rows['item_code']; ?></td>
                    <td><?php echo $rows['part_no']; ?></td>
                    <td><?php echo $rows['item']; ?></td>
                    <td><?php echo $rows['qty'].' '.$rows['unit']; ?></td>
                    <td><?php echo rp($rows['unit_price']); ?></td>
                    <td><?php echo $rows['disc']; ?></td>
                    <td><?php echo rp($rows['total']); ?></td>
                    <td class="text-center">
                        <a 
                            href="<?php echo '../itemform/'.$rows['id_po_items'].'/edit'; ?>" 
                            style="padding:10px" 
                            data-toggle="tooltip" 
                            data-placement="top" 
                            title="" 
                            data-original-title="Edit item">
                                <i class="material-icons">mode_edit</i></a>

                        <a 
                            href="<?php echo '../delete_item/'.$rows['id_po_items']; ?>" 
                            style="padding:10px" 
                            data-toggle="tooltip" 
                            data-placement="top" 
                            title="" 
                            data-original-title="Delete item" 
                            onclick="return confirm('Are you sure want to delete this?');">
                                <i class="material-icons text-danger">delete</i></a>
                    </td>
                </tr>  
                <?php $no++; ?>
            <?php endforeach ?>
            
        </tbody>
    </table>
</div>