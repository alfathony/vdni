$(function () {
    $('.js-basic-example').DataTable({
        "responsive": true,
        "order": [[ 1, "desc" ]],
        "columnDefs": [
            { "orderable": false, "targets": 0 }
          ]
    });

    //Exportable table
    $('.js-exportable').DataTable({
        dom: 'Bfrtip',
        responsive: true,
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
});