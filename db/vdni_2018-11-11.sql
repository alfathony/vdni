# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.6.41)
# Database: vdni
# Generation Time: 2018-11-11 03:56:08 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table bill_of_lading
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bill_of_lading`;

CREATE TABLE `bill_of_lading` (
  `id_bl` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_po` int(11) DEFAULT NULL,
  `no_bl` varchar(20) DEFAULT NULL,
  `shipper_name` varchar(100) DEFAULT NULL,
  `consignee` varchar(100) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `vessel` varchar(50) DEFAULT NULL,
  `voyage_no` varchar(10) DEFAULT NULL,
  `port_of_loading` varchar(50) DEFAULT NULL,
  `port_of_discharge` varchar(50) DEFAULT NULL,
  `notify_party` varchar(100) DEFAULT NULL,
  `no_original_bl` varchar(20) DEFAULT NULL,
  `master_name` varchar(100) DEFAULT NULL,
  `is_deleted` tinyint(2) DEFAULT '0',
  PRIMARY KEY (`id_bl`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table bill_of_lading_container
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bill_of_lading_container`;

CREATE TABLE `bill_of_lading_container` (
  `id_bl_container` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_po_item` int(11) DEFAULT NULL,
  `number_container` varchar(100) DEFAULT NULL,
  `type_container` varchar(100) DEFAULT NULL,
  `seal` varchar(20) DEFAULT NULL,
  `ton` decimal(10,0) DEFAULT NULL,
  `ton_checked` decimal(10,0) DEFAULT NULL,
  `is_deleted` tinyint(2) DEFAULT '0',
  PRIMARY KEY (`id_bl_container`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table mr_vendor
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mr_vendor`;

CREATE TABLE `mr_vendor` (
  `id_vendor` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `vendor_name` varchar(255) DEFAULT NULL,
  `address` text,
  `telephone` varchar(16) DEFAULT NULL,
  `pic_name` varchar(255) DEFAULT NULL,
  `handphone` varchar(16) DEFAULT NULL,
  `fax` varchar(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `is_active` tinyint(2) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_deleted` tinyint(2) DEFAULT '0',
  PRIMARY KEY (`id_vendor`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `mr_vendor` WRITE;
/*!40000 ALTER TABLE `mr_vendor` DISABLE KEYS */;

INSERT INTO `mr_vendor` (`id_vendor`, `vendor_name`, `address`, `telephone`, `pic_name`, `handphone`, `fax`, `email`, `is_active`, `created_at`, `updated_at`, `is_deleted`)
VALUES
	(1,'Mega Anugrah Energi, PT','Jl. Jendral Sudiman, Menara BCA building. Lt 2','08212799988','','','','',1,NULL,'2018-11-09 15:12:55',0),
	(2,'Pertamina Persero, PT','Jl. Merdeka Utara, Gambir, Jakarta Barat','021 787899','Hendra','08787878888','021 7627726 ext 2','hendra@domain.com',1,NULL,'2018-11-09 15:25:21',0),
	(3,'Sinarmas Dwi Tama, PT','Jl Magelang raya no 21. Magelang Jawa Tengah','021 8988899','Jojo Suherman','0877700090','021 343434','sinarmas@domain.com',0,'2018-11-09 14:38:00','2018-11-09 14:55:46',0);

/*!40000 ALTER TABLE `mr_vendor` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table purchase_order
# ------------------------------------------------------------

DROP TABLE IF EXISTS `purchase_order`;

CREATE TABLE `purchase_order` (
  `id_po` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `status` int(1) DEFAULT NULL,
  `po_no` varchar(255) DEFAULT NULL,
  `po_date` date DEFAULT NULL,
  `id_vendor` int(11) DEFAULT NULL,
  `delivery_time` varchar(255) DEFAULT NULL,
  `payment_method` varchar(255) DEFAULT NULL,
  `payment_term` varchar(255) DEFAULT NULL,
  `note` text,
  `discount` int(11) DEFAULT '0',
  `vat` int(3) DEFAULT '0',
  `downpayment` int(11) DEFAULT NULL,
  `grand_total` int(11) DEFAULT NULL,
  `receiver_name` varchar(255) DEFAULT NULL,
  `receiver_date` date DEFAULT NULL,
  `prefered_by` varchar(255) DEFAULT NULL,
  `approved_by` varchar(255) DEFAULT NULL,
  `is_deleted` int(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_po`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `purchase_order` WRITE;
/*!40000 ALTER TABLE `purchase_order` DISABLE KEYS */;

INSERT INTO `purchase_order` (`id_po`, `status`, `po_no`, `po_date`, `id_vendor`, `delivery_time`, `payment_method`, `payment_term`, `note`, `discount`, `vat`, `downpayment`, `grand_total`, `receiver_name`, `receiver_date`, `prefered_by`, `approved_by`, `is_deleted`, `created_at`, `updated_at`)
VALUES
	(3,3,'9000','2018-10-29',2,'2018-11-09','','','edit',0,0,0,9000,'Alfathony','2018-10-25','gunawan','Alfathony',0,NULL,'2018-11-09 10:15:08'),
	(4,2,'1019000','2018-10-29',1,'2018-11-09','bank transfer','2 month',NULL,20,20,20,400000,'alfa','2018-10-30','alfat','alfath',0,NULL,NULL),
	(5,4,'01','2018-11-04',2,'2018-11-04','bank transfer','2 month','hallo ini catatan untuk ngetes status dan created at  dan edit update at',10,10,10,2310,'Alfathony','2018-11-04','alfat','Alfathony',0,'2018-11-03 22:01:57','2018-11-09 10:18:30'),
	(6,3,'04100','2018-11-04',1,'2018-11-04','bank transfer','2 month','ini pt mega',10,10,10,120000,'Alfathony','0000-00-00','gunawan','Alfathony',0,'2018-11-04 06:27:39','2018-11-04 21:36:56'),
	(7,1,'02','2018-11-05',2,'2018-11-05','Klik BCA','1 Tahun','ini adalah contoh note yang di ubah',10,10,10,900,'toni','2018-11-05','toni','toni',0,'2018-11-05 01:11:38','2018-11-05 01:13:15'),
	(8,1,'800','2018-11-09',0,'2018-11-09','','','',0,0,0,NULL,'','2018-11-09','','',0,'2018-11-08 21:58:10','2018-11-09 10:30:05'),
	(9,1,'1000/VII/2018/PO/VDNI','2018-11-09',1,'2018-11-09','Kartu Kredit','1 Tahun','ini adalah PO yang mana menggunakan masking di downpayment',10,10,20,40000,'Anton','2018-11-09','apa nih','Alfathony',0,'2018-11-09 14:10:51','2018-11-09 14:13:05');

/*!40000 ALTER TABLE `purchase_order` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table purchase_order_items
# ------------------------------------------------------------

DROP TABLE IF EXISTS `purchase_order_items`;

CREATE TABLE `purchase_order_items` (
  `id_po_items` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_po` int(11) DEFAULT NULL,
  `item_code` int(11) DEFAULT NULL,
  `part_no` int(11) DEFAULT NULL,
  `item` varchar(500) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `unit` varchar(20) DEFAULT '',
  `unit_price` int(11) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `disc` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_deleted` int(1) DEFAULT '0',
  PRIMARY KEY (`id_po_items`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `purchase_order_items` WRITE;
/*!40000 ALTER TABLE `purchase_order_items` DISABLE KEYS */;

INSERT INTO `purchase_order_items` (`id_po_items`, `id_po`, `item_code`, `part_no`, `item`, `qty`, `unit`, `unit_price`, `total`, `disc`, `created_at`, `updated_at`, `is_deleted`)
VALUES
	(1,1,9008991,1,'Lian (Kapur)',600,'ton',3200000,132000000,0,NULL,NULL,0),
	(2,6,123,0,'Kapur',10,'ton',100,900,10,'2018-11-04 19:51:28','2018-11-05 01:06:19',1),
	(3,6,0,2,'Batubara',10,'ton',12000,120000,0,'2018-11-04 19:54:32','2018-11-05 01:06:55',0),
	(4,6,908,908,'Timah',10,'ton',2000,18000,10,'2018-11-04 19:59:35',NULL,1),
	(5,5,20,20,'Lian Kapur ubah lagi',20,'meter',20,320,20,'2018-11-04 21:45:51','2018-11-05 00:56:57',0),
	(6,5,100,100,'Batubara',1,'ton',1100,990,10,'2018-11-04 22:06:19','2018-11-05 00:44:57',0),
	(7,6,10,10,'Tembaga',1,'ton',110,99,10,'2018-11-05 01:00:50','2018-11-05 01:01:02',1),
	(8,6,1212,12,'Timah',1,'ton',10000,10000,0,'2018-11-05 01:07:21','2018-11-05 01:07:47',1),
	(9,3,123123,123123,'Kapur',1,'ton',0,0,0,'2018-11-05 01:08:50','2018-11-09 10:52:08',0),
	(10,7,1212,1212,'Batubara',1,'ton',1000,900,10,'2018-11-05 01:14:57',NULL,0),
	(11,5,231,12312,'Batubara',1,'ton',1000,1000,0,'2018-11-09 10:16:20',NULL,0),
	(12,3,8099,909,'Itembaru',1,'1',0,0,0,'2018-11-09 10:50:04',NULL,0),
	(13,3,2323,23,'Batubara pake masking',1,'1',10000,9000,10,'2018-11-09 14:08:15',NULL,0),
	(14,9,1001,1001,'Ini item pertama',1,'ton',20000,20000,0,'2018-11-09 14:13:30','2018-11-09 14:14:38',0),
	(15,9,1002,1002,'Ini item kedua',2,'ton',10000,20000,0,'2018-11-09 14:13:59',NULL,0),
	(16,9,1003,1003,'Ini akan di hapus',1,'1',5000,5000,0,'2018-11-09 14:15:13',NULL,1);

/*!40000 ALTER TABLE `purchase_order_items` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
